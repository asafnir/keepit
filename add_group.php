<?php

require './db.php';
require './fb.php';

global $db;
global $facebook;
$user = storeUser($facebook->getUser());
$category = $db->getRow('select * from category where user_id=?i and name=?s', $facebook->getUser(), $_GET['group']);

if($category){
    echo json_encode(array('error' => 'Already exists!'));
    exit();
}

$category = $db->query('insert into category(user_id, name) values (?i, ?s)', $facebook->getUser(), $_GET['group']);
$category = $db->insertId();
$category = $db->getRow('select * from category where catid=?i', $category);

echo json_encode(array('category'=>$category));