<?php

require './db.php';

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if(!$_GET['user_id']){
    echo json_encode(array('error' => 'Variable user_id missing!'));
    exit();
}

global $db;
$groups = $db->getAll('select * from category where user_id = ?i and name != ?s', $_GET['user_id'], 'Without Group');

echo json_encode($groups? $groups : array());
