<?php
require 'lib/safemysql.class.php';
require_once 'fb.php';
$opts = array(
    'host' => 'localhost',
    'user' => 'thegoon2_keepit',
    'db' => 'thegoon2_keepit',
    'pass' => 'keepit123',
    'charset' => 'latin1'
);
$db = new SafeMySQL($opts); // with some of the default settings overwritten

function storeUser($id, $name = ''){
    global $db;
    $result = $db->query('insert into fb_users(user_id, fio)
                 values(?i, ?s)
                 on DUPLICATE KEY UPDATE fio = VALUES(fio)', $id, $name);

    if(!$result){
        echo json_encode(array('error' => 'storeUser failed: ' . mysql_error()));
        exit();
    }

    $result = $db->getRow('select * from fb_users where user_id = ?i', $id);

    return $result;
}

function storePost($user_id, $name, $group, $actor, $target_fbid, $link, $html, $fb_post){
    global $db;
    global $facebook;
    /*try{
        $fb_post = $facebook->api('/' . $actor . '_' . $target_fbid);
        $fb_post = json_encode($fb_post);
    } catch (FacebookApiException $e) {
        //echo json_encode(array('error' => $e->getMessage()));
        //exit();
        $fb_post = '';
    }*/
    
    $cat = $db->getRow('select * from category where user_id = ?i and name = ?s', $user_id, $group);

    if(!$cat){
        $db->query('insert into category(user_id, name) values (?i,?s)', $user_id, $group);
        global $cat;
        $cat = $db->getRow('select * from category where user_id = ?i and name = ?s', $user_id, $group);
    }

    $db->query('insert into posts(name, catid, actor, target_fbid, user_id, content, link, html)
                values (?s, ?i, ?s, ?s, ?i, ?s, ?s, ?s)',
                $name, $cat['catid'], $actor, $target_fbid, $user_id, $fb_post, $link, $html);

    $post = $db->getRow('select * from posts where postid = ?i', $db->insertId());

    return $post;
}