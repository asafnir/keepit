<?php
require './fb.php';
require './db.php';
global $facebook;

$user = $facebook->getUser();
if ($user) {
    try {
        // Proceed knowing you have a logged in user who's authenticated.
        $user_profile = $facebook->api('/me');
    } catch (FacebookApiException $e) {
        $user = null;
    }
}
?>

<!doctype html>
<html>
    <head>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="css/custom-theme/jquery-ui-1.9.2.custom.css" rel="stylesheet">
        <link rel="stylesheet" href="css/L3c21GsVmIp.css"/>
        <link rel="stylesheet" href="css/-j5xOUitBLI.css"/>
        <link href="css/main.css" rel="stylesheet">
        <script src="js/jquery.js"></script>
        <script src="js/jquery-ui-1.9.2.custom.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/mustache.min.js"></script>
        <script src="js/main.js"></script>
    </head>
    <body>
        <div id="ajax"><img src="images/ajax-loader.gif" /></div>
        <?php if ($user) { ?>
            <div class="container">
                <div id="logo"><a href="http://thegoodeed.com/keep/widget/extension.php" target="_blank" title="Show in new window"><span class="keep-it-button">keep.it</span></a></div>
                <div class="row">
                    <!--<div class="fb-login-button" autologoutlink="true"></div>-->
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="tabbable tabs-left">
                                <ul class="nav nav-tabs">
                                    <?php
                                    $category = $db->getAll('select * from category where user_id=?i', $user);
                                    foreach($category as $item){
                                    ?>
                                    <li><a href="#group<?php print $item['catid'];?>" data-toggle="tabajax"><?php print $item['name'];?></a></li>
                                    <?php }?>
                                    <li><a href="#new" data-toggle="tab">New Item</a></li>
                                </ul>
                                <div class="tab-content">
                                    <?php
                                    foreach($category as $item){?>
                                    <div class="tab-pane" id="group<?php print $item['catid'];?>"></div>
                                    <?php } ?>
                                    <div class="tab-pane" id="new">
                                        <form class="form-inline" role="form">
                                            <div class="form-group">
                                                <label class="sr-only" for="name">Name</label>
                                                <input type="text" class="form-control" id="name" name="name" placeholder="Enter name">
                                            </div>
                                            <button type="submit" class="btn btn-default">Add</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="contextMenuCat" class="dropdown clearfix" style="position: absolute;display:none;">
                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu" style="display:block;position:static;margin-bottom:5px;">
                    <li><a tabindex="-1" href="#" class="delete">Delete</a></li>
                </ul>
            </div>
        <?php

        } else { ?>
            <div class="container"><div class="fb-login-button" scope="read_stream"></div></div>
        <?php } ?>
        <script>
            window.fbAsyncInit = function() {
                FB.init({
                    appId: '<?php echo $facebook->getAppID() ?>',
                    cookie: true,
                    xfbml: true,
                    oauth: true
                });
                FB.Event.subscribe('auth.login', function(response) {
                    window.location.reload();
                });
                FB.Event.subscribe('auth.logout', function(response) {
                    window.location.reload();
                });
            };
            (function() {
                var e = document.createElement('script'); e.async = true;
                e.src = document.location.protocol +
                    '//connect.facebook.net/en_US/all.js';
                document.getElementsByTagName('body')[0].appendChild(e);
            }());
        </script>
    </body>
</html>