<?php

header('Access-Control-Allow-Origin: chrome-extension://'.$_POST['origin']);
header('Access-Control-Allow-Credentials: true');

require_once './db.php';
require_once './fb.php';

global $facebook;

//print $facebook->getAccessToken();
//print $facebook->getLoginUrl();

header('Content-Type: application/json');

if(!$_POST['user_id']){
    echo json_encode(array('error' => 'Variable user_id missing!'));
    exit();
}

$user = storeUser($_POST['user_id']);
$post = storePost($user['user_id'], $_POST['name'], $_POST['group'], $_POST['actor'], $_POST['target_fbid'], $_POST['link'], $_POST['html'], $_POST['fb_post']);

echo json_encode(array('ok' => 'ok', 'user' => $user, 'post' => $post));