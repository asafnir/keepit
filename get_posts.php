<?php

header('Access-Control-Allow-Origin: *');

require './db.php';

header('Content-Type: application/json');

if(!$_GET['group']){
    echo json_encode(array('error' => 'Variable group missing!'));
    exit();
}
global $db;
$posts_req = $db->getAll('select * from posts where catid=?i', $_GET['group']);

require_once './fb.php';

$queries = array();

foreach ($posts_req as $post_req) {
    $url = '/' . $post_req['actor'] . '_' . $post_req['target_fbid'] . "?fields=likes.limit(1).summary(true),comments.limit(1).summary(true),from,picture,name,description,message";
    $queries[] = array('method' => 'GET', 'relative_url' => $url);
}

$batchResponse = $facebook->api('?batch='.json_encode($queries), 'POST');

$posts = array();

$actor_ids = array();
foreach ($batchResponse as $response) {
    $post = json_decode($response['body'], true);
    if (!empty($post['from']['id'])) {
        $from_id = $post['from']['id'];
        $actor_i = array_search($from_id, $actor_ids);
        if ($actor_i === FALSE) {
            $actor_ids[] = $from_id;
        }
    }
    $posts[] = $post;
}

$queries = array();

foreach ($actor_ids as $actor_id) {
    $url = '/' . $actor_id;
    $queries[] = array('method' => 'GET', 'relative_url' => $url);
}

$batchResponse = $facebook->api('?batch='.json_encode($queries), 'POST');

foreach ($batchResponse as $response) {
    $actor = json_decode($response['body'], true);
    $actor_id = $actor['id'];
    foreach ($posts as &$post) {
        if (!empty($post['from']['id'])) {
            $from_id = $post['from']['id'];
            if ($actor_id === $from_id) {
                $post['actor'] = $actor;
            }
        }
    }
}

echo json_encode($posts);