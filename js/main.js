$(function(){

    events($('[data-toggle="tabajax"]'));

    $('body').on("click", function() {
        $('.cm').removeClass('cm');
        $('#contextMenuCat').hide();
    });

    $('#contextMenuCat').on('click', 'a', function(){
        console.log(this, $('.cm'));
        if($(this).hasClass('delete')){
            var g = $('.cm').attr('href').replace('#group', '');
            $.getJSON('delete.php', {group: $('.cm').attr('href').replace('#group', '')}, function(response){
                $('#group'+g).remove();
                $('[href="#group'+g+'"]').parent().remove();
            });
        }
    });

    $('#new form').on('submit', function(){
        var $this = $(this);
        if(this.name.value==''){
            alert('Enter name!');
            return false;
        }

        $('button', this).prop('disabled', true).addClass('loading');

        $.getJSON('add_group.php', {group: this.name.value}, function(response){
            $('button', $this).prop('disabled', false).removeClass('loading');
            if(response.error){
                alert(response.error);
                return;
            }
            $('input', $this).val('');

            $('<li><a href="#group'+response.category.catid+'" data-toggle="tabajax">'+response.category.name+'</a></li>')
                .insertBefore($('li.active'));
            $('.tab-content').append('<div class="tab-pane" id="group'+response.category.catid+'"></div>');
            events($('[href="#group'+response.category.catid+'"]'));
        });
        return false;
    });

    function clickPost(){
        var data = $(this).data('keep');
        console.log(data);
        /*if(!data.data){
            window.open('https://www.facebook.com/'+data.link.replace('https://www.facebook.com/', ''));
        } else {
            var html = Mustache.render(post_tmpl, data.data);
            $(html)
                .dialog({
                    width: 400,
                    resizable: false
                });
        }*/
        $(data.html)
            .dialog({
                width: 400,
                resizable: false
            });
    }

    function events($el){
        $el
            .on('click', function(e) {
                e.preventDefault();
                $this = $(this);
                $($this.attr('href')).html('<div style="text-align: center;"><img src="./images/loader.gif"></div></div>');
                $this.tab('show');
                $.getJSON('get_posts.php',{group: $this.attr('href').replace('#group','')}, function(posts){
                    var html = '<div><table class="table"><thead><tr><th>#<th>Name<th>Post<tbody>';
                    var panel = $($this.attr('href'));
                    panel.html('<table class="table"><thead><tr><th>#<th>Name<th>Post<tbody>');
                    $.each(response, function(key, value){
                        //var data = value.content?JSON.parse(value.content):undefined;
                        $('table', panel).append(
                            $('<tr data-id="'+value.postid+'"><td>'+ ( key + 1 ) + '<td>' + value.name + '<td>' + value.content.substr(0, 40) + '</tr>')
                                .data('keep', {link: value.link, html: value.html})
                                .click(clickPost)
                                .draggable({helper: function(){return $(this).clone().css({'background': 'white', width: '300px'})}})
                        );
                    });
                    //$this.tab('show');
                });
            })
            .on("contextmenu", function(e) {
                $(this).addClass('cm');
                $('#contextMenuCat').css({
                    display: "block",
                    left: e.pageX,
                    top: e.pageY
                });
                return false;
            })
            .parent().droppable({
                drop: function( event, ui ) {
                    var tr = ui.draggable,
                        cat = event.target;
                    if($(cat).hasClass('active')) return;
                    $.getJSON('move.php', {group: $('a', cat).attr('href').replace('#group', ''), postid: tr.attr('data-id')}, function(response){
                        tr.remove();
                    });
                }
            })
    }

    window.fbAsyncInit = function() {
        FB.init({
            appId: '200724046777884',
            cookie: true,
            xfbml: true,
            oauth: true
        });
        FB.Event.subscribe('auth.login', function(response) {
            window.location.reload();
        });
        FB.Event.subscribe('auth.logout', function(response) {
            window.location.reload();
        });
    };
    (function() {
        var e = document.createElement('script'); e.async = true;
        e.src = document.location.protocol +
            '//connect.facebook.net/en_US/all.js';
        document.getElementsByTagName('head')[0].appendChild(e);
    }());

    $( document ).ajaxStart(function() {
        $('#ajax').show();
    }).ajaxStop(function() {
        $('#ajax').hide();
    });
});

var post_tmpl = '<div class="post type_{{type}}" data-id="{{id}}">\
    <div class="clearfix">\
        <div class="bw clearfix">\
            <div class="post_header clearfix">\
                <a href="#{{from.id}}" class="post_actor_pic">\
                    <img src="http://graph.facebook.com/{{from.id}}/picture" />\
                </a>    \
                <a href="#{{from.id}}">{{from.name}}</a> \
                {{^message_tags}}\
                    {{#to.data.0}}\
                        <span class="light small" style="font-size: 10px;">&nbsp;▶&nbsp;</span> <a href="#{{id}}">{{name}}</a>\
                    {{/to.data.0}}\
                {{/message_tags}}\
                <div class="meta">\
                    <a class="ago" href="#{{id}}">{{ago}}</a> \
                    {{#application}}\
                        via {{name}}\
                    {{/application}}\
                </div>\
            </div>\
            <div class="post_text">\
                {{story}}\
                {{message}}\
                {{#is_photo}}\
                    {{name}}\
                {{/is_photo}}\
            \
            {{#place}}\
                <span class="light">\
                &mdash; at </span> <a href="#{{id}}" class="thin">{{name}}</a>\
            {{/place}}\
            </div>\
            {{#link}}\
                <div class="link mtm {{#is_photo}}link_photo{{/is_photo}}">\
                    {{#position}}\
                        <a href="{{link}}" target="_blank">\
                            <img src="{{source}}" class="pic_wide" data-link="{{link}}" />\
                        </a>\
                    {{/position}}\
                    {{^position}}\
                        {{#picture}}\
                            <a href="{{link}}" target="_blank">\
                                <img src="{{picture}}" class="link_picture" data-link="{{link}}" />\
                            </a>\
                        {{/picture}}\
                        {{^is_photo}}\
                        <div class="tcell">\
                            <div style="word-break:break-word;">\
                                <a href="{{link}}" target="_blank">{{name}}</a>\
                                {{#caption}}\
                                    <div class="mts small light">{{caption}}</div>\
                                {{/caption}}\
                                {{#description}}\
                                    <div class="mts small">{{description}}</div>\
                                {{/description}}\
                            </div>\
                        </div>\
                        {{/is_photo}}\
                    {{/position}}\
                </div>\
            {{/link}}\
            {{^link}}\
                {{#place}}\
                    {{#location}}\
                        <img src="http://maps.googleapis.com/maps/api/staticmap?center={{latitude}},{{longitude}}&zoom=14&size=330x120&sensor=false&markers=color:0xB06582%7C{{latitude}},{{longitude}}" width="330" height="120" class="mtm" />\
                    {{/location}}\
                {{/place}}\
            {{/link}}\
        </div>\
        <div class="mtm feedback">\
            <div class="light info">\
                <span class="actions">\
                    {{#canLike}}\
                       <a href="#">Like</a> \
                    {{/canLike}}\
                </span>\
            </div>\
            \
            {{>likes}}\
            {{>comments}}\
        </div>\
    </div>\
</div>';